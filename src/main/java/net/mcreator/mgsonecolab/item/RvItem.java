
package net.mcreator.mgsonecolab.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BucketItem;

import net.mcreator.mgsonecolab.init.MgsoneColabModFluids;

public class RvItem extends BucketItem {
	public RvItem() {
		super(() -> MgsoneColabModFluids.RV,
				new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1).rarity(Rarity.COMMON).tab(CreativeModeTab.TAB_MISC));
		setRegistryName("rv_bucket");
	}
}
