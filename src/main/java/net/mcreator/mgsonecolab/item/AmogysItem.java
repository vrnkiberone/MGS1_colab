
package net.mcreator.mgsonecolab.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;

public class AmogysItem extends Item {
	public AmogysItem() {
		super(new Item.Properties().tab(CreativeModeTab.TAB_MISC).stacksTo(64).rarity(Rarity.RARE));
		setRegistryName("amogys");
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 0;
	}
}
