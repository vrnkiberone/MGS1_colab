
package net.mcreator.mgsonecolab.item;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;

public class LavandaPickaxeItem extends PickaxeItem {
	public LavandaPickaxeItem() {
		super(new Tier() {
			public int getUses() {
				return 500;
			}

			public float getSpeed() {
				return 4f;
			}

			public float getAttackDamageBonus() {
				return 1098f;
			}

			public int getLevel() {
				return 60;
			}

			public int getEnchantmentValue() {
				return 5;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.EMPTY;
			}
		}, 1, 11f, new Item.Properties().tab(CreativeModeTab.TAB_TOOLS));
		setRegistryName("lavanda_pickaxe");
	}
}
