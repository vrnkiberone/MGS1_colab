
package net.mcreator.mgsonecolab.fluid;

import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fluids.FluidAttributes;

import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.particles.ParticleOptions;

import net.mcreator.mgsonecolab.init.MgsoneColabModItems;
import net.mcreator.mgsonecolab.init.MgsoneColabModFluids;
import net.mcreator.mgsonecolab.init.MgsoneColabModBlocks;

public abstract class RvFluid extends ForgeFlowingFluid {
	public static final ForgeFlowingFluid.Properties PROPERTIES = new ForgeFlowingFluid.Properties(() -> MgsoneColabModFluids.RV,
			() -> MgsoneColabModFluids.FLOWING_RV,
			FluidAttributes.builder(new ResourceLocation("mgsone_colab:blocks/rw"), new ResourceLocation("mgsone_colab:blocks/rw"))

	).explosionResistance(10f).canMultiply()

			.bucket(() -> MgsoneColabModItems.RV_BUCKET).block(() -> (LiquidBlock) MgsoneColabModBlocks.RV);

	private RvFluid() {
		super(PROPERTIES);
	}

	@Override
	public ParticleOptions getDripParticle() {
		return ParticleTypes.EXPLOSION;
	}

	public static class Source extends RvFluid {
		public Source() {
			super();
			setRegistryName("rv");
		}

		public int getAmount(FluidState state) {
			return 8;
		}

		public boolean isSource(FluidState state) {
			return true;
		}
	}

	public static class Flowing extends RvFluid {
		public Flowing() {
			super();
			setRegistryName("flowing_rv");
		}

		protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder) {
			super.createFluidStateDefinition(builder);
			builder.add(LEVEL);
		}

		public int getAmount(FluidState state) {
			return state.getValue(LEVEL);
		}

		public boolean isSource(FluidState state) {
			return false;
		}
	}
}
