
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.mgsonecolab.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.core.Registry;

import net.mcreator.mgsonecolab.world.features.plants.LavandagrassFeature;
import net.mcreator.mgsonecolab.world.features.plants.LavandaFeature;
import net.mcreator.mgsonecolab.world.features.plants.HolyGrassFeature;
import net.mcreator.mgsonecolab.world.features.lakes.RvFeature;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class MgsoneColabModFeatures {
	private static final Map<Feature<?>, FeatureRegistration> REGISTRY = new HashMap<>();
	static {
		REGISTRY.put(HolyGrassFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.VEGETAL_DECORATION, HolyGrassFeature.GENERATE_BIOMES,
				HolyGrassFeature.CONFIGURED_FEATURE));
		REGISTRY.put(LavandaFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.VEGETAL_DECORATION, LavandaFeature.GENERATE_BIOMES,
				LavandaFeature.CONFIGURED_FEATURE));
		REGISTRY.put(RvFeature.FEATURE,
				new FeatureRegistration(GenerationStep.Decoration.LAKES, RvFeature.GENERATE_BIOMES, RvFeature.CONFIGURED_FEATURE));
		REGISTRY.put(LavandagrassFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.VEGETAL_DECORATION,
				LavandagrassFeature.GENERATE_BIOMES, LavandagrassFeature.CONFIGURED_FEATURE));
	}

	@SubscribeEvent
	public static void registerFeature(RegistryEvent.Register<Feature<?>> event) {
		event.getRegistry().registerAll(REGISTRY.keySet().toArray(new Feature[0]));
		REGISTRY.forEach((feature, registration) -> Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, feature.getRegistryName(),
				registration.configuredFeature()));
	}

	@Mod.EventBusSubscriber
	private static class BiomeFeatureLoader {
		@SubscribeEvent
		public static void addFeatureToBiomes(BiomeLoadingEvent event) {
			for (FeatureRegistration registration : REGISTRY.values()) {
				if (registration.biomes() == null || registration.biomes().contains(event.getName())) {
					event.getGeneration().getFeatures(registration.stage()).add(() -> registration.configuredFeature());
				}
			}
		}
	}

	private static record FeatureRegistration(GenerationStep.Decoration stage, Set<ResourceLocation> biomes,
			ConfiguredFeature<?, ?> configuredFeature) {
	}
}
