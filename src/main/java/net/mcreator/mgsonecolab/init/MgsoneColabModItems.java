
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.mgsonecolab.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.mgsonecolab.item.RvItem;
import net.mcreator.mgsonecolab.item.LavandaseedsItem;
import net.mcreator.mgsonecolab.item.LavandaextractItem;
import net.mcreator.mgsonecolab.item.LavandaPickaxeItem;
import net.mcreator.mgsonecolab.item.LavandaGemItem;
import net.mcreator.mgsonecolab.item.GgvpItem;
import net.mcreator.mgsonecolab.item.AmogysItem;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class MgsoneColabModItems {
	private static final List<Item> REGISTRY = new ArrayList<>();
	public static final Item SAKURA_WOOD = register(MgsoneColabModBlocks.SAKURA_WOOD, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item SAKURA_LOG = register(MgsoneColabModBlocks.SAKURA_LOG, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item SAKURA_PLANKS = register(MgsoneColabModBlocks.SAKURA_PLANKS, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item SAKURA_LEAVES = register(MgsoneColabModBlocks.SAKURA_LEAVES, CreativeModeTab.TAB_DECORATIONS);
	public static final Item SAKURA_STAIRS = register(MgsoneColabModBlocks.SAKURA_STAIRS, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item SAKURA_SLAB = register(MgsoneColabModBlocks.SAKURA_SLAB, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item SAKURA_FENCE = register(MgsoneColabModBlocks.SAKURA_FENCE, CreativeModeTab.TAB_DECORATIONS);
	public static final Item SAKURA_FENCE_GATE = register(MgsoneColabModBlocks.SAKURA_FENCE_GATE, CreativeModeTab.TAB_REDSTONE);
	public static final Item SAKURA_PRESSURE_PLATE = register(MgsoneColabModBlocks.SAKURA_PRESSURE_PLATE, CreativeModeTab.TAB_REDSTONE);
	public static final Item SAKURA_BUTTON = register(MgsoneColabModBlocks.SAKURA_BUTTON, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item HOLY_GRASS = register(MgsoneColabModBlocks.HOLY_GRASS, CreativeModeTab.TAB_DECORATIONS);
	public static final Item LAVANDA = register(MgsoneColabModBlocks.LAVANDA, CreativeModeTab.TAB_DECORATIONS);
	public static final Item AMOGYS = register(new AmogysItem());
	public static final Item RV_BUCKET = register(new RvItem());
	public static final Item LAVANDAEXTRACT = register(new LavandaextractItem());
	public static final Item LAVANDASEEDS = register(new LavandaseedsItem());
	public static final Item LAVANDA_PICKAXE = register(new LavandaPickaxeItem());
	public static final Item LAVANDAGRASS = register(MgsoneColabModBlocks.LAVANDAGRASS, CreativeModeTab.TAB_DECORATIONS);
	public static final Item LAVANDA_GEM = register(new LavandaGemItem());
	public static final Item LAVANDA_TREE_WOOD = register(MgsoneColabModBlocks.LAVANDA_TREE_WOOD, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item GGVP = register(new GgvpItem());
	public static final Item LAVANDA_TREE_LOG = register(MgsoneColabModBlocks.LAVANDA_TREE_LOG, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item LAVANDA_TREE_PLANKS = register(MgsoneColabModBlocks.LAVANDA_TREE_PLANKS, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item LAVANDA_TREE_LEAVES = register(MgsoneColabModBlocks.LAVANDA_TREE_LEAVES, CreativeModeTab.TAB_DECORATIONS);
	public static final Item LAVANDA_TREE_STAIRS = register(MgsoneColabModBlocks.LAVANDA_TREE_STAIRS, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item LAVANDA_TREE_SLAB = register(MgsoneColabModBlocks.LAVANDA_TREE_SLAB, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item LAVANDA_TREE_FENCE = register(MgsoneColabModBlocks.LAVANDA_TREE_FENCE, CreativeModeTab.TAB_DECORATIONS);
	public static final Item LAVANDA_TREE_FENCE_GATE = register(MgsoneColabModBlocks.LAVANDA_TREE_FENCE_GATE, CreativeModeTab.TAB_REDSTONE);
	public static final Item LAVANDA_TREE_PRESSURE_PLATE = register(MgsoneColabModBlocks.LAVANDA_TREE_PRESSURE_PLATE, CreativeModeTab.TAB_REDSTONE);
	public static final Item LAVANDA_TREE_BUTTON = register(MgsoneColabModBlocks.LAVANDA_TREE_BUTTON, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item LAVANDABLOCK = register(MgsoneColabModBlocks.LAVANDABLOCK, CreativeModeTab.TAB_BUILDING_BLOCKS);

	private static Item register(Item item) {
		REGISTRY.add(item);
		return item;
	}

	private static Item register(Block block, CreativeModeTab tab) {
		return register(new BlockItem(block, new Item.Properties().tab(tab)).setRegistryName(block.getRegistryName()));
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Item[0]));
	}
}
