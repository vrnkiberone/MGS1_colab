
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.mgsonecolab.init;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.block.Block;

import net.mcreator.mgsonecolab.block.SakuraWoodBlock;
import net.mcreator.mgsonecolab.block.SakuraStairsBlock;
import net.mcreator.mgsonecolab.block.SakuraSlabBlock;
import net.mcreator.mgsonecolab.block.SakuraPressurePlateBlock;
import net.mcreator.mgsonecolab.block.SakuraPlanksBlock;
import net.mcreator.mgsonecolab.block.SakuraLogBlock;
import net.mcreator.mgsonecolab.block.SakuraLeavesBlock;
import net.mcreator.mgsonecolab.block.SakuraFenceGateBlock;
import net.mcreator.mgsonecolab.block.SakuraFenceBlock;
import net.mcreator.mgsonecolab.block.SakuraButtonBlock;
import net.mcreator.mgsonecolab.block.RvBlock;
import net.mcreator.mgsonecolab.block.LavandagrassBlock;
import net.mcreator.mgsonecolab.block.LavandablockBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeWoodBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeStairsBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeSlabBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treePressurePlateBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treePlanksBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeLogBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeLeavesBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeFenceGateBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeFenceBlock;
import net.mcreator.mgsonecolab.block.Lavanda_treeButtonBlock;
import net.mcreator.mgsonecolab.block.LavandaBlock;
import net.mcreator.mgsonecolab.block.HolyGrassBlock;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class MgsoneColabModBlocks {
	private static final List<Block> REGISTRY = new ArrayList<>();
	public static final Block SAKURA_WOOD = register(new SakuraWoodBlock());
	public static final Block SAKURA_LOG = register(new SakuraLogBlock());
	public static final Block SAKURA_PLANKS = register(new SakuraPlanksBlock());
	public static final Block SAKURA_LEAVES = register(new SakuraLeavesBlock());
	public static final Block SAKURA_STAIRS = register(new SakuraStairsBlock());
	public static final Block SAKURA_SLAB = register(new SakuraSlabBlock());
	public static final Block SAKURA_FENCE = register(new SakuraFenceBlock());
	public static final Block SAKURA_FENCE_GATE = register(new SakuraFenceGateBlock());
	public static final Block SAKURA_PRESSURE_PLATE = register(new SakuraPressurePlateBlock());
	public static final Block SAKURA_BUTTON = register(new SakuraButtonBlock());
	public static final Block HOLY_GRASS = register(new HolyGrassBlock());
	public static final Block LAVANDA = register(new LavandaBlock());
	public static final Block RV = register(new RvBlock());
	public static final Block LAVANDAGRASS = register(new LavandagrassBlock());
	public static final Block LAVANDA_TREE_WOOD = register(new Lavanda_treeWoodBlock());
	public static final Block LAVANDA_TREE_LOG = register(new Lavanda_treeLogBlock());
	public static final Block LAVANDA_TREE_PLANKS = register(new Lavanda_treePlanksBlock());
	public static final Block LAVANDA_TREE_LEAVES = register(new Lavanda_treeLeavesBlock());
	public static final Block LAVANDA_TREE_STAIRS = register(new Lavanda_treeStairsBlock());
	public static final Block LAVANDA_TREE_SLAB = register(new Lavanda_treeSlabBlock());
	public static final Block LAVANDA_TREE_FENCE = register(new Lavanda_treeFenceBlock());
	public static final Block LAVANDA_TREE_FENCE_GATE = register(new Lavanda_treeFenceGateBlock());
	public static final Block LAVANDA_TREE_PRESSURE_PLATE = register(new Lavanda_treePressurePlateBlock());
	public static final Block LAVANDA_TREE_BUTTON = register(new Lavanda_treeButtonBlock());
	public static final Block LAVANDABLOCK = register(new LavandablockBlock());

	private static Block register(Block block) {
		REGISTRY.add(block);
		return block;
	}

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Block[0]));
	}

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			HolyGrassBlock.registerRenderLayer();
			LavandaBlock.registerRenderLayer();
			LavandagrassBlock.registerRenderLayer();
		}
	}
}
