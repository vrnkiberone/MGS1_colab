
package net.mcreator.mgsonecolab.block;

import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.entity.Entity;
import net.minecraft.core.BlockPos;

import net.mcreator.mgsonecolab.procedures.RvKoghdaMobighrokStalkivaietsiaSBlokomProcedure;
import net.mcreator.mgsonecolab.init.MgsoneColabModFluids;

public class RvBlock extends LiquidBlock {
	public RvBlock() {
		super(MgsoneColabModFluids.RV, BlockBehaviour.Properties.of(Material.WATER).strength(10f)

		);
		setRegistryName("rv");
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) {
		return true;
	}

	@Override
	public void entityInside(BlockState blockstate, Level world, BlockPos pos, Entity entity) {
		super.entityInside(blockstate, world, pos, entity);
		RvKoghdaMobighrokStalkivaietsiaSBlokomProcedure.execute(entity);
	}
}
